# How to Build an API With Ruby on Rails

## Overview
This API is built with Ruby on Rails and is using PostgreSQL database, is a simple API that allows users to create, read, update, and delete .
## Getting Started
Follow these steps to set up and run the API:

1. Clone this repository to your local machine.

2. In the `.env` file put all your environment variables from PostgreSQL.

3. Run following commands to build and run the API:
    
```bash
docker-compose build
docker-compose up -d
```

4. Run the following commands to create, migrate and seed the database:

```bash
docker-compose exec app rails db:create
docker-compose exec app rails db:migrate
docker-compose exec app rails db:seed
```

5. Open Postman and copy the `ISV3-95.postman_collection.json` file into it to verify the following endpoints.

#### API Endpoints:
---
###### * GET /api/v1/products: Get a list of all products.
###### * GET /api/v1/products/:id: Get a single product.
###### * POST /api/v1/products: Create a new product.
###### * PATCH /api/v1/products/:id: Update a product.
###### * DELETE /api/v1/products/:id: Delete a product.



