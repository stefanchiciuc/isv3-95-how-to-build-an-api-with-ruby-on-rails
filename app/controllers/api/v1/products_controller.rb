# frozen_string_literal: true

module Api
  module V1
    class ProductsController < ApplicationController
      def index
        products = Product.all
        render json: products, status: 200
      end

      def create
        product = Product.new(
          name: prod_params[:name],
          brand: prod_params[:brand],
          price: prod_params[:price],
          description: prod_params[:description]
        )
        if product.save
          render json: product, status: 200
        else
          render json: { error: 'Error creating review' }
        end
      end

      def show
        product = Product.find_by(id: params[:id])
        if product
          render json: product, status: 200
        else
          render json: { error: 'Product not found' }
        end
      end

      def update
        product = Product.find_by(id: params[:id])
        product.update(prod_params)
        render json: product, status: 200
      end

      def destroy
        product = Product.find_by(id: params[:id])
        product.destroy
        render json: product, status: 200
      end

      private

      def prod_params
        params.require(:product).permit(%i[
                                          name
                                          brand
                                          price
                                          description
                                        ])
      end
    end
  end
end
