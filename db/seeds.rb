# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
Product.create([
                 {
                   name: 'PS5',
                   brand: 'Sony',
                   price: '$499.99',
                   description: 'The PlayStation 5 is an upcoming home video game console developed by Sony Interactive Entertainment. Announced in 2019 as the successor to the PlayStation 4, it is scheduled to launch in late 2020. The platform is anticipated to launch in two varieties, as a base PlayStation 5 system incorporating an Ultra HD Blu-ray compatible optical disc drive for retail game support alongside digital distribution via the PlayStation Store, and a lower-cost Digital variant lacking the disc drive while retaining digital download support.'
                 },
                 {
                   name: 'Xbox Series X',
                   brand: 'Microsoft',
                   price: '$499.99',
                   description: 'The Xbox Series X is an upcoming home video game console developed by Microsoft. Announced during E3 2019, the console is scheduled for release in late 2020. The Series X is planned to launch with Halo Infinite as a launch title, and is expected to be backwards compatible with all games and controllers that work on the Xbox One. The console will also feature hardware-accelerated ray tracing, and variable rate shading.'
                 },
                 {
                   name: 'Nintendo Switch',
                   brand: 'Nintendo',
                   price: '$299.99',
                   description: "The Nintendo Switch is a video game console developed by Nintendo, released worldwide in most regions on March 3, 2017. It is a hybrid console that can be used as both a stationary and portable device. Its wireless Joy-Con controllers, with standard buttons and directional analog sticks for user input, motion sensing, and tactile feedback, can attach to both sides of the console to support handheld-style play. They can also connect to a Grip accessory to provide a traditional home console gamepad form, or be used individually in the hand like the Wii Remote and Nunchuk, supporting local multiplayer modes. The Nintendo Switch's software supports online gaming through Internet connectivity, as well as local wireless ad hoc connectivity with other Switch consoles. Nintendo Switch games and software are available on both physical flash-based ROM cartridges and digital distribution via Nintendo eShop; the system has no region lockout."
                 },
                 {
                   name: 'PlayStation 4',
                   brand: 'Sony',
                   price: '$299.99',
                   description: "The PlayStation 4 (officially abbreviated as PS4) is an eighth-generation home video game console developed by Sony Interactive Entertainment. Announced as the successor to the PlayStation 3 in February 2013, it was launched on November 15 in North America, November 29 in Europe, South America and Australia, and on February 22, 2014 in Japan. A console of the eighth generation, it competes with Microsoft's Xbox One and Nintendo's Wii U and Switch."
                 },
                 {
                   name: 'Xbox One',
                   brand: 'Microsoft',
                   price: '$299.99',
                   description: "The Xbox One is a line of home video game consoles developed by Microsoft. Announced in May 2013, it is the successor to Xbox 360 and the third base console in the Xbox series of video game consoles. It was first released in North America, parts of Europe, Australia, and South America in November 2013, and in Japan, China, and other European countries in September 2014. It is the first Xbox game console to be released in China, specifically in the Shanghai Free-Trade Zone. Microsoft marketed the device as an all-in-one entertainment system, hence the name 'Xbox One'. An eighth-generation console, it mainly competed against Sony's PlayStation 4 and Nintendo's Wii U and later the Switch."
                 }
               ])
